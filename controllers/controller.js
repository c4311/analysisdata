const {json} = require('body-parser')
const model = require('../models/model')
const msgs = require('./strings')
const chartHandler = require('./chartExport')
require('dotenv').config()


const controller = {}

controller.compareSales = async (req, res) => {
    let result = {}
    gameone = req.body.gameone
    gametwo = req.body.gametwo
    try {
        let game1 = await model.getSalesBaseOnGame(gameone)
        let game2 = await model.getSalesBaseOnGame(gametwo)
        if (game1.success && game1.rows && game2.success && game2.rows) {
            let g1info = {
                data: game1.rows[0],
                name: gameone
            }
            let g2info = {
                data: game2.rows[0],
                name: gametwo
            }
            let info = {g1info, g2info}
            let o = createChartConf(info, 0)
            let chartFile = chartHandler.createChart(o.series, o.categories, o.chartType)
            res.status(msgs.rc_Successful);
            // result.data=[{path:process.env.LISTEN_HOST+":"+process.env.LISTEN_PORT+chartFile.slice(1)}]
            result.data = chartFile
        } else {
            result.msg = msgs.msg_errorDB_en
            res.status(msgs.rc_InternalServerError);
        }
    } catch (error) {
        result.msg = msgs.msg_errorCONT_en
        res.status(msgs.rc_InternalServerError);
        console.log(error)
    }
    res.send(JSON.stringify(result))
}

controller.compareSalesBaseOnYear = async (req, res) => {
    let result = {}
    startyear = req.body.startyear
    stopyear = req.body.stopyear
    try {
        let tmp = await model.getSalesBaseOnYear(startyear, stopyear)
        if (tmp.success && tmp.rows) {
            let info = {
                data: tmp.rows,
                name: 'sales'
            }
            let o = createChartConf(info, 1)
            let chartFile = chartHandler.createChart(o.series, o.categories, o.chartType)
            res.status(msgs.rc_Successful);
            // result.data=[{path:process.env.LISTEN_HOST+":"+process.env.LISTEN_PORT+chartFile.slice(1)}]
            result.data = chartFile
        } else {
            result.msg = msgs.msg_errorDB_en
            res.status(msgs.rc_InternalServerError);
        }
    } catch (error) {
        result.msg = msgs.msg_errorCONT_en
        res.status(msgs.rc_InternalServerError);
        console.log(error)
    }
    res.send(JSON.stringify(result))
}

controller.compareSalesBaseOnPublisher = async (req, res) => {
    let result = {}
    startyear = req.body.startyear
    stopyear = req.body.stopyear
    publisher1 = req.body.publisherone
    publisher2 = req.body.publishertwo
    try {
        let tmp = await model.getSalesBaseOnPublisher(startyear, stopyear, publisher1, publisher2)
        if (tmp.success && tmp.rows) {
            let info = {
                data: tmp.rows,
                name: 'sales'
            }
            o = createChartConf(info, 2)
            let chartFile = chartHandler.createChart(o.series, o.categories, o.chartType)
            res.status(msgs.rc_Successful);
            // result.data=[{path:process.env.LISTEN_HOST+":"+process.env.LISTEN_PORT+chartFile.slice(1)}]
            result.data = chartFile
        } else {
            result.msg = msgs.msg_errorDB_en
            res.status(msgs.rc_InternalServerError);
        }
    } catch (error) {
        result.msg = msgs.msg_errorCONT_en
        res.status(msgs.rc_InternalServerError);
        console.log(error)
    }
    res.send(JSON.stringify(result))
}

controller.compareSalesBaseOnGenre = async (req, res) => {
    let result = {}
    startyear = req.body.startyear
    stopyear = req.body.stopyear
    try {
        let tmp = await model.getSalesBaseOnGenre(startyear, stopyear)
        if (tmp.success && tmp.rows) {
            let info = {
                data: tmp.rows,
                name: 'sales'
            }
            let o = createChartConf(info, 3)
            let chartFile = chartHandler.createChart(o.series, o.categories, o.chartType)
            res.status(msgs.rc_Successful);

            //result.data=[{path:process.env.LISTEN_HOST+":"+process.env.LISTEN_PORT+chartFile.slice(1)}]
            result.data = chartFile
        } else {
            result.msg = msgs.msg_errorDB_en
            res.status(msgs.rc_InternalServerError);
        }
    } catch (error) {
        result.msg = msgs.msg_errorCONT_en
        res.status(msgs.rc_InternalServerError);
        console.log(error)
    }
    res.send(JSON.stringify(result))
}

function createChartConf(info, mode) {
    let output = {}
    output.series = []
    if (mode === 0) {//moghayese froosh 2 bazi
        console.log(info.g1info.data);
        Object.keys(info.g1info.data).forEach((element, index) => {
            let tmp = {}
            tmp.name = Object.keys(info.g1info.data)[index]
            tmp.data = [parseFloat(info.g1info.data[tmp.name]), parseFloat(info.g2info.data[tmp.name])]
            output.series.push(tmp)
        });
        output.categories = [info.g1info.name, info.g2info.name]
        output.chartType = 'bar'
    } else if (mode === 1) {// moghayese yek baze zamani sotoni
        info.data.forEach(element => {
            let tmp = {}
            tmp.name = element['Year']
            tmp.data = [parseFloat(element['sum'])]
            output.series.push(tmp)
        });
        output.chartType = 'bar'
    } else if (mode === 2) {// bar asase publisher ha khati
        let tmp1 = {}
        tmp1.name = info.data[0]['Publisher']
        tmp1.data = []
        let tmp2 = {}
        tmp2.name = info.data[1]['Publisher']
        tmp2.data = []
        let categories = []
        info.data.forEach(element => {
            if (element['Publisher'] === tmp1.name) {
                tmp1.data.push(parseFloat(element['sum']))
            } else {
                tmp2.data.push(parseFloat(element['sum']))
            }
            if (!categories.includes(element['Year'])) {
                categories.push(element['Year'])
            }
        });
        output.series = [tmp1, tmp2]
        output.categories = categories
        output.chartType = 'line'
    } else if (mode === 3) {//bar asas ganr sotoni
        info.data.forEach(element => {
            let tmp = {}
            tmp.name = element['Genre']
            tmp.data = [parseFloat(element['sum'])]
            output.series.push(tmp)
        });
        output.chartType = 'bar'
    }
    //console.log(output);
    return output

}

module.exports = controller