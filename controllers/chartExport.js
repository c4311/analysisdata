const fs = require("fs");
// const chartExporter = require("highcharts-export-server");

// chartExporter.initPool();

chartHandler={}

// chartHandler.drawAndSave= function (chartOptions,outputFile){
    
//     chartExporter.export({
//         type: "png",
//         options: chartOptions,
//         width: 1200
//     }, (err, res) => {
//         let imageb64 = res.data;
//         fs.writeFileSync(outputFile, imageb64, "base64");
//         console.log(`The chart ${outputFile} has been succesfully generated!`);
//         chartExporter.killPool();
//     });
// }

chartHandler.createChart = function createChart(series,categories,chartType){
    let chartOptions = {
        chart: {
            type: chartType
        },
        title: {
            text: 'Compare Sales'
        },
        subtitle: {
            text: 'Source: vgsales'
        },
        xAxis: {
            categories: categories,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'sales',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: '#FFFFFF',
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: series
    };
    // let outputfile='./charts/2-'+String(Date.now())+'.png'
    // chartHandler.drawAndSave(chartOptions,outputfile)
    // return outputfile
    return chartOptions
}

module.exports=chartHandler