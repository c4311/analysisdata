const axios = require("axios");
require('dotenv').config();
const strings = require('./strings');

var myAuthenticator = {}

myAuthenticator.verifyUser = async function(req, res, next) {
    try {
        req.middleware = {};
        if (req.headers.token === undefined) {
            res.status(403);
            res.send({
                status: strings.rc_Unauthorized,
                message: strings.msg_errorUnauthorized1
            })
            return
        }
        var response = await axios.post(`http://${process.env.AUTHENTICATION_HOST}:${process.env.AUTHENTICATION_PORT}/api/verify`, {
            token: req.headers.token
        });

        if (response.data != null && response.data.data[0] != null && response.data.data[0].verifed === true) {
            next();
        }
        else {
            res.status(403);
            res.send({
                status: strings.rc_Unauthorized,
                message: strings.msg_errorUnauthorized2
            })
        }

    } catch (error) {
        res.status(500);
        res.send({
            status: strings.rc_InternalServerError,
            message: strings.msg_errorReceiveToken
        })
    }
}

module.exports = myAuthenticator;