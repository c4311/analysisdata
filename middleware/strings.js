module.exports = {
    msg_errorJWT_en: 'error in verify jwt',
    msg_errorUnauthorized1: 'Unauthorized (token does not exists)',
    msg_errorUnauthorized2: 'Unauthorized (token is not valid)',
    msg_errorReceiveToken: 'Error in receiving token header of the request',
    rc_InternalServerError: 500,
    rc_Successful: 200,
    rc_Unauthorized: 403,
    rc_NotFound: 404,
    rc_Redirect: 301
}