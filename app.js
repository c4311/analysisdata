const express = require('express');
const bodyParser =require('body-parser') ;
const routes = require('./routes')
require('dotenv').config()

const app = express()
app.use(bodyParser.json())
app.use('/api',routes)
app.use('/charts',express.static('charts'))

module.exports=app