const express = require('express');
const controller = require('./controllers/controller')
const middleware = require('./middleware/authenticator2')

const routes = express();

routes.post('/compare/sales', middleware.verifyUser, controller.compareSales)
routes.post('/compare/year' , middleware.verifyUser, controller.compareSalesBaseOnYear)
routes.post('/compare/publisher', middleware.verifyUser, controller.compareSalesBaseOnPublisher)
routes.post('/compare/genre', middleware.verifyUser,controller.compareSalesBaseOnGenre)


module.exports = routes