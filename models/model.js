const pool = require('pg')
require('dotenv').config()


var poolins = new pool.Pool({
    user: process.env.DATABASE_USER,
    host: process.env.DATABASE_HOST,
    database: process.env.DATABASE,
    password: process.env.DATABASE_PASSWORD,
    port: process.env.DATABASE_PORT,
    ssl: false,
})

const model = {}

model.getSalesBaseOnGame = async function (gamename) {
    let result = {}
    try {
        let tmp = await poolins.query('select "NA_Sales","EU_Sales","JP_Sales","Other_Sales","Global_Sales" from vgsales where "Name"=$1', [gamename]);
        result.success = true
        result.rows = tmp.rows
    } catch (error) {
        result.success = false
        result.error = error
        console.log(error)
    }
    return result
}

model.getSalesBaseOnYear = async function (startyear, stopyear) {
    let result = {}
    try {
        let tmp = await poolins.query('select sum("Global_Sales"),"Year" from vgsales where "Year">=$1 and "Year"<=$2 group by "Year" order by "Year";', [startyear, stopyear]);
        result.success = true
        result.rows = tmp.rows
    } catch (error) {
        result.success = false
        result.error = error
        console.log(error)
    }
    return result
}

model.getSalesBaseOnPublisher = async function (startyear, stopyear, publisher1, publisher2) {
    let result = {}
    try {
        let tmp = await poolins.query('select sum("Global_Sales"),"Year","Publisher" from vgsales where ("Year"<=$1 and "Year">=$2) and ("Publisher"=$3 Or "Publisher"=$4) group by "Year","Publisher" order by "Year","Publisher";', [stopyear, startyear, publisher1, publisher2]);
        result.success = true
        result.rows = tmp.rows
    } catch (error) {
        result.success = false
        result.error = error
        console.log(error)
    }
    return result
}

model.getSalesBaseOnGenre = async function (startyear, stopyear) {
    let result = {}
    try {
        let tmp = await poolins.query('select sum("Global_Sales"),"Genre" from vgsales where ("Year"<=$1 and "Year">=$2) group by "Genre" order by "Genre";', [stopyear, startyear]);
        result.success = true
        result.rows = tmp.rows
    } catch (error) {
        result.success = false
        result.error = error
        console.log(error)
    }
    return result
}

module.exports = model